package config

// MetricsConfig contains Prometheus metrics endpoint settings
type MetricsConfig struct {
	ListenAddr string `envconfig:"METRICS_LISTEN_ADDR" required:"true" default:"0.0.0.0:8080"`
}
