package config

import (
	"github.com/evalphobia/logrus_sentry"
	"github.com/getsentry/raven-go"
	"github.com/natefinch/lumberjack"
	"github.com/sirupsen/logrus"
	"os"
)

// LoggerConfig contains mapping of environment variable names to config values
type LoggerConfig struct {
	Level      string `envconfig:"LOGGER_LEVEL"      required:"false" default:"info"` // Debug, Info, Warning, Error, Fatal, Panic
	Filename   string `envconfig:"LOGGER_FILENAME"   required:"false" default:"/var/log/gozar.log"`
	MaxSize    int    `envconfig:"LOGGER_MAXSIZE"    required:"false" default:"100"`
	MaxBackups int    `envconfig:"LOGGER_MAXBACKUPS" required:"false" default:"35"`
	MaxAge     int    `envconfig:"LOGGER_MAXAGE"     required:"false" default:"35"`
}

func SetupLoggerConfig(config *LoggerConfig) error {
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp:   true,
		TimestampFormat: "2006-01-02 15:04:05",
	})

	logLevel, err := logrus.ParseLevel(config.Level)
	if err != nil {
		return err
	}

	logrus.SetLevel(logLevel)
	logrus.SetOutput(os.Stdout)
	logrus.StandardLogger().Out = os.Stdout

	shHook, err := logrus_sentry.NewWithClientSentryHook(raven.DefaultClient, []logrus.Level{
		logrus.PanicLevel,
		logrus.FatalLevel,
		logrus.ErrorLevel,
	})

	if err == nil {
		logrus.AddHook(shHook)
	}

	lumberjackLogger := &lumberjack.Logger{
		Filename:   config.Filename,
		MaxSize:    config.MaxSize,
		MaxBackups: config.MaxBackups,
		MaxAge:     config.MaxAge,
		LocalTime:  true,
	}

	// Add Lumberjack hook
	ljHook, err := NewLumberjackHook(lumberjackLogger)
	if err != nil {
		logrus.Fatalln("Unable to add the Lumberjack hook :", err)
	} else {
		logrus.AddHook(ljHook)
	}

	return err
}
