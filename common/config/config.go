package config

import (
	"github.com/kelseyhightower/envconfig"
)

const EnvProduction = "production"
const EnvDevelopment = "development"
const EnvTest = "test"

// BaseConfig contains general config such log level and app env
type BaseConfig struct {
	AppEnv string `envconfig:"APP_ENV" required:"false" default:"development"`
}

func Process(envPrefix string, config interface{}) {
	err := envconfig.Process(envPrefix, config)
	if err != nil {
		panic(err)
	}
}
