package config

import (
	"github.com/miekg/dns"
	"net"
	"time"
)

// ResolverConfig contains upstream servers and resolution-related configuration
type ResolverConfig struct {
	Client *dns.Client

	Upstreams []string      `envconfig:"RESOLVER_UPSTREAMS" required:"true" default:"8.8.8.8:853,1.1.1.1:853"`
	Timeout   time.Duration `envconfig:"RESOLVER_TIMEOUT"   required:"true" default:"20s"`
}

func SetupResolverConfig(config *ResolverConfig) error {
	config.Client = &dns.Client{
		Net: "tcp-tls",
		Dialer: &net.Dialer{
			Timeout: time.Second * 20,
		},
		ReadTimeout:  config.Timeout,
		WriteTimeout: config.Timeout,
	}

	return nil
}
