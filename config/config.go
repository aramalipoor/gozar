package config

import (
	commonConfig "gitlab.com/aramalipoor/gozar/common/config"
)

const envPrefix = "GOZAR"

// AppConfig contains mapping of environment variable names to config values
type AppConfig struct {
	// Common configs
	*commonConfig.BaseConfig
	*commonConfig.LoggerConfig
	*commonConfig.MetricsConfig
	*commonConfig.ResolverConfig

	// App common configs
	ListenAddr string `envconfig:"LISTEN_ADDR" required:"true"  default:"0.0.0.0:5353"`
}

// Config is the application config global
var Config = &AppConfig{}

func init() {
	commonConfig.Process(envPrefix, Config)

	if err := commonConfig.SetupLoggerConfig(Config.LoggerConfig); err != nil {
		panic(err)
	}

	if err := commonConfig.SetupResolverConfig(Config.ResolverConfig); err != nil {
		panic(err)
	}
}
