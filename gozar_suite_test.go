package gozar

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestPortalApi(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Gozar Suite")
}
