package main

import (
	"github.com/miekg/dns"
	"github.com/sirupsen/logrus"
	"gitlab.com/aramalipoor/gozar/config"
	"gitlab.com/aramalipoor/gozar/handler"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	udpServer := &dns.Server{Addr: config.Config.ListenAddr, Net: "udp"}
	tcpServer := &dns.Server{Addr: config.Config.ListenAddr, Net: "tcp"}

	dns.HandleFunc(".", handler.Handle)

	go func() {
		logrus.Printf("Listening for UDP DNS requests on address %s", config.Config.ListenAddr)
		if err := udpServer.ListenAndServe(); err != nil {
			logrus.Fatalf("UDP listen error: %s\n", err)
		}
	}()

	go func() {
		logrus.Printf("Listening for TCP DNS requests on address %s", config.Config.ListenAddr)
		if err := tcpServer.ListenAndServe(); err != nil {
			logrus.Fatalf("TCP listen error: %s\n", err)
		}
	}()

	// Shutdown gracefully
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	logrus.Println("Shutting down servers...")

	if err := udpServer.Shutdown(); err != nil {
		logrus.Fatal("UDP server shutdown error:", err)
	}

	if err := tcpServer.Shutdown(); err != nil {
		logrus.Fatal("TCP server shutdown error:", err)
	}

	logrus.Println("Bye!")
}
