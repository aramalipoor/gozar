#
# 1. Build Container
#
FROM golang:1.11.1 AS build

ENV TZ=UTC \
    GO111MODULE=on

RUN apt-get update && \
    apt-get install -y \
        tzdata \
        curl \
        bash \
        ca-certificates \
        rsync \
        git \
        make \
    && \
    rm -rf /var/lib/apt/lists/* && \
    cp --remove-destination /usr/share/zoneinfo/${TZ} /etc/localtime && \
    echo "${TZ}" > /etc/timezone && \
    mkdir -p /src

WORKDIR /src

# First add modules list to better utilize caching
COPY go.sum go.mod /src/

# Download dependencies
RUN go mod download

COPY . /src

# Build components.
# Put built binaries and runtime resources in /app dir ready to be copied over or used.
RUN go install ./cmd/* && \
    mkdir -p /app && \
    cp -r $GOPATH/bin/* ./resources /app/

#
# 2. Runtime Container
#
FROM debian:stable-slim

ENV TZ=UTC

RUN apt-get update && \
    apt-get install -y \
        tzdata \
        bash \
        ca-certificates \
    && \
    rm -rf /var/lib/apt/lists/* && \
    cp --remove-destination /usr/share/zoneinfo/${TZ} /etc/localtime && \
    echo "${TZ}" > /etc/timezone

WORKDIR /app

EXPOSE 53

COPY --from=build /app /app/

CMD ["sh", "-c", "echo 'Please specify a starting command: \n'$(ls -d -1 /app/* | grep -v '/resources$')"]
