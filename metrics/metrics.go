package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"sync"
)

var (
	DNSQueriesTotal = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "gozar",
		Name:      "dns_queries_total",
		Help:      "Number of served queries",
	}, []string{"qclass", "qtype", "qname"})

	DNSRequestDuration = prometheus.NewSummary(prometheus.SummaryOpts{
		Name:       "dns_request_duration",
		Help:       "DNS requests latency in microseconds",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
	})
)

var registerMetrics sync.Once

// Register all metrics.
func init() {
	registerMetrics.Do(func() {
		prometheus.MustRegister(DNSQueriesTotal)
		prometheus.MustRegister(DNSRequestDuration)
	})
}
