package tests

import (
	"fmt"
	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/aramalipoor/gozar/controllers"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"time"
)

var _ = Describe("UserController", func() {
	var (
		req        *http.Request
		respWriter *httptest.ResponseRecorder
		resp       *http.Response
		respBody   string
	)

	Describe("Local time of covilha", func() {

		BeforeEach(func() {
			gin.SetMode(gin.TestMode)

			r := gin.Default()
			r.GET("/tehran", controllers.BuildTimeGetter("Tehran, Iran", "Asia/Tehran"))

			respWriter = httptest.NewRecorder()

			req, _ = http.NewRequest("GET", "/tehran", nil)
			r.ServeHTTP(respWriter, req)
			resp = respWriter.Result()
			defer resp.Body.Close()

			b, _ := ioutil.ReadAll(resp.Body)
			respBody = string(b)

			fmt.Print(string(respBody))
		})

		It("returns a 200", func() {
			Expect(resp.StatusCode).To(Equal(200))
		})

		It("returns the correct body", func() {
			loc, _ := time.LoadLocation("Asia/Tehran")
			realCurrentTime := time.Now().In(loc)

			fmt.Print(string(respBody))

			Expect(string(respBody)).To(ContainSubstring(realCurrentTime.Format("15:04")))
		})
	})
})
