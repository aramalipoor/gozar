package tests

import (
	_ "gitlab.com/aramalipoor/gozar/config"
	"os"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestControllers(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Controllers Suite")
}

var _ = BeforeSuite(func() {
	os.Setenv("APP_ENV", "test")

	// This is needed as test must right from projects root, to use relative paths for ./resources directory
	os.Chdir("..")
})

var _ = AfterSuite(func() {

})
