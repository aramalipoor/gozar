# DNS Forwarder

A simple DNS forwarder that calls upstream servers over TLS i.e. DNSSEC, DNS over TLS

## Local Deployment

1. Clone this repository.
2. Push this repository into your own Gitlab account so the app is tested and image is built.
3. Create a Kubernetes cluster using [minikube](https://kubernetes.io/docs/setup/minikube/), if you don't already have a Kubernetes cluster running.
4. Install [Helm Tiller](https://docs.helm.sh/install/) in your Kubernetes cluster.
5. Enable ingress addon in Minikube:
  ```sh
  minikube addons enable ingress
  ```
6. Create a new namespace:
  ```sh
  kubectl create namespace gozar
  ``` 
7. Create a new Gitlab token in Settings > Repository > Deploy Tokens and copy the values.
8. Create `gitlab-registry` secret so that Kubernetes can pull the image from container registry:
  ```sh
  kubectl create secret -n gozar docker-registry gitlab-registry \
    --docker-server=registry.gitlab.com \
    --docker-username=gitlab+deploy-token-xxxxxxx \
    --docker-password=$GITLAB_DEPLOY_TOKEN_KEY \
    --docker-email=aram.alipoor@gmail.com
  ```
9. Make sure local domain is resolvable:
  ```sh
  echo "$(minikube ip) gozar.local"
  
  # Make sure above result is in hosts file:
  # sudo vi /etc/hosts
  ```
10. Install `gozar` micro-service using Helm charts:
  ```sh
  helm install ./helm \
    --name gozar \
    --namespace gozar \
    --values ./helm/values-local.yaml \
    --set image.repository=registry.gitlab.com/aramalipoor/gozar
  ```
## Production Deployment
This section assumes you have a running Kubernetes cluster for production with an already configured ingress controller and Helm Tiller already installed in a cluster-wide setup.

Note that only collaborators who have their keys added to git-crypt will have access to production values file i.e. `helm/production/` dir.

### First installation
0. Run Gitlab CI pipeline to build `gozar/gozar` image based on this repository.
1. Create new application's namespace:
    ```sh
    kubectl create namespace gozar
    ``` 
2. Install `gozar` micro-service using Helm charts:
    ```sh
    helm install ./helm \
      --name gozar \
      --namespace gozar \
      --values ./helm/production/values.yaml
    ```
3. When Gozar is deployed you can use it in any pod:
  * `nameserver gozar.gozar.svc.cluster.local.`
### Updating with a new version
After pushing a new tag (e.g. `v2.0.0`), Gitlab should automatically build the image, then you can run command below:
  ```sh
  kubectl -n gozar set image deployment/gozar web=registry.gitlab.com/gozar/gozar:v2.0.0 --record
  ```

## Future Improvements

Below sections describes how we can further improve deployment of this application.

### DNS Module
* Cache DNS queries respecting TTL
* Return immediately with corresponding errors if all nameservers failed with errors
* Re-use connections to upstream nameservers to reduce the delay in heavy environments 

### CI/CD Pipeline
* Separate build and output image for each micro-service.
* Create a temporary environment on all or some specific branches (e.g. `release/xxxx`, `RC-xxx`) based on Helm charts, so that QA team or the developers can confirm a release. This temporary env will be destroyed after a deadline or manually by the release master.
* First build the Docker image and run tests against that image so that if tests are successful the **same image** will be promoted without any changes. This will make sure the environment where the tests have ran against (libraries, configs etc.) will stay the same and we won't be surprised on production.
* Use Gitlab's native integration with Kubernetes when releasing master branch as latest staging env.
* Add release checklist and notes regarding production deployments and upgrades.
* Use git-crypt to encrypt all `secrets` and `production` directories then only allow release masters to view these files. 
* Enable developers to build image locally so they don't need to use Gitlab's remote container registry. 

## Kubernetes Deployment
* Improve templates so that dev, test, staging and prod will use the same Helm charts just with a different `values-*.yml` file.
* Based on the real production nodes topology add anti-affinity rules to deployment pods so we're sure they are distributed in different failure zones. (e.g. racks, baremetals, nodes etc.) 
* Automate seeding the initial database so that dev and staging envs can benefit quick and easy deployments.
* Add TLS support to Ingress and Service
* Avoid using Helm charts for production as upgrades will be risky and Helm has limitations.
