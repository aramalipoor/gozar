package handler

import (
	"github.com/miekg/dns"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"gitlab.com/aramalipoor/gozar/config"
	"gitlab.com/aramalipoor/gozar/metrics"
	"time"
)

func Handle(w dns.ResponseWriter, req *dns.Msg) {
	logger := logrus.WithFields(logrus.Fields{
		"requestId": req.Id,
		"qName":     req.Question[0].Name,
		"qClass":    req.Question[0].Qclass,
		"qType":     req.Question[0].Qtype,
	})

	logger.Debugln("New request to handle")

	if len(req.Question) == 0 {
		logger.Debugln("DNS request does not contain any 'question'", req)
		dns.HandleFailed(w, req)
		return
	}

	// Metrics
	timer := prometheus.NewTimer(prometheus.ObserverFunc(func(v float64) {
		us := v * 1000 // make milliseconds
		metrics.DNSRequestDuration.Observe(us)
	}))
	defer timer.ObserveDuration()

	metrics.DNSQueriesTotal.With(prometheus.Labels{
		"qname":  req.Question[0].Name,
		"qclass": string(req.Question[0].Qclass),
		"qtype":  string(req.Question[0].Qtype),
	}).Inc()

	// Querying upstreams
	resp := make(chan *dns.Msg, 1)
	timeout := make(chan bool, 1)

	go func() {
		time.Sleep(config.Config.ResolverConfig.Timeout)
		timeout <- true
	}()

	for _, upstream := range config.Config.ResolverConfig.Upstreams {
		go resolveBy(upstream, req, resp)
	}

	select {
	case r := <-resp:
		logger.Debugf("Resolved with answers: %s", r.Answer)
		w.WriteMsg(r)
		return
	case <-timeout:
		logger.Errorln("Handling DNS request timed out")
		dns.HandleFailed(w, req)
		return
	}
}

func resolveBy(upstream string, request *dns.Msg, response chan *dns.Msg) {
	logger := logrus.WithFields(logrus.Fields{
		"upstream":  upstream,
		"requestId": request.Id,
		"qName":     request.Question[0].Name,
		"qClass":    request.Question[0].Qclass,
		"qType":     request.Question[0].Qtype,
	})

	logger.Infof("Resolving %s", request.Question[0].Name)
	resp, _, err := config.Config.ResolverConfig.Client.Exchange(request, upstream)

	metrics.DNSQueriesTotal.With(prometheus.Labels{
		"qname":  request.Question[0].Name,
		"qclass": string(request.Question[0].Qclass),
		"qtype":  string(request.Question[0].Qtype),
	}).Inc()

	if err != nil {
		logger.Debugln("Could not handle DNS request", err.Error())
	} else {
		logger.Debugln("Successful resolution", resp.Answer)
		response <- resp
	}
}
